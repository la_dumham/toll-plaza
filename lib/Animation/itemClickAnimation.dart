import 'package:flutter/material.dart';

class ItemClickAnimation extends StatefulWidget {
  final Widget child;
  final double scaleValue;
  final Widget route;
  ItemClickAnimation({
    Key key,
    this.child,
    @required this.scaleValue,
    this.route
  });

  @override
  _ItemClickAnimationState createState() => _ItemClickAnimationState();
}

class _ItemClickAnimationState extends State<ItemClickAnimation> {
  double scaleValue = 1.0;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

  }
  @override
  Widget build(BuildContext context) {
    return Transform.scale(
      scale: scaleValue,
      child: GestureDetector(
        onTap: (){
          setState(() {
            scaleValue = widget.scaleValue;
            Future.delayed(Duration(milliseconds: 100)).then((value) {
              setState(() {
                scaleValue = 1.0;
              });
              if(widget.route != null)
                Navigator.push(context, MaterialPageRoute(builder: (_)=>widget.route));
            });



          });
        },
          child: widget.child
      ),
    );
  }
}
